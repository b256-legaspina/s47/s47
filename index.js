// document-querySelector -> use it to retrieve an element from the webpage

/*
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-input');
	document.getElementsByTagName('input');
*/
const textFirstName = document.querySelector('#txt-first-name');
const textLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


//Activity 47 (one Function)

function displayFullName(event) {
	const firstName = textFirstName.value;
	const lastName = textLastName.value;

	spanFullName.textContent = `${firstName} ${lastName}`;
}

textFirstName.addEventListener('keyup', displayFullName);
textLastName.addEventListener('keyup', displayFullName);







//Initial Try
// action is considered as an event
// textFirstName.addEventListener('keyup', (event) => {
// 	const firstName = textFirstName.value;
// 	const lastName = textLastName.value;

// 	spanFullName.innerHTML = `${firstName} ${lastName}`;

// });

// textLastName.addEventListener('keyup', (event) => {
// 	const firstName = textFirstName.value;
// 	const lastName = textLastName.value;

// 	spanFullName.innerHTML = `${firstName} ${lastName}`;

// });

// textFirstName.addEventListener('keyup', (event) => {
// 	console.log(event.target);
// 	console.log(event.target.value);
// })

// textLastName.addEventListener('keyup', (event) => {
// 	spanFullName.
// })



